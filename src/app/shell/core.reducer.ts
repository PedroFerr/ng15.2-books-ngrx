import {createReducer, on} from '@ngrx/store';
import {ShellState} from "./core.selector";
import { CatalogPageActions } from '../features-modules/catalog/store/catalog-page.actions';

export const initialState: ShellState = {
  cartOpen: false
};

export const shellReducer = createReducer(
  initialState,
  on(CatalogPageActions.openCart, () => ({
    cartOpen: true
  }))
);
