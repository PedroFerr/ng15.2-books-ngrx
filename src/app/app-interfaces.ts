
export interface CartItem {
    id: string;
    item: Item;
    qty: number;
}

export interface Item {
    id: string;
    name: string;
    description: string;
    price: number;
    imageUrl: string;
}
