import { Component, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ShellState, selectNumberOfCartItems } from './shell/core.selector';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSidenav } from "@angular/material/sidenav";
import { Item } from './app-interfaces';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
    title = 'ng15.1-books-ngrx';

    numberOfCartItems$: Observable<number>;
    private snackbarSubscription: Subscription | undefined;

    constructor(private store: Store<ShellState>, private matsnackbar: MatSnackBar) {
        this.numberOfCartItems$ = this.store.select(selectNumberOfCartItems);
    }

    onItemAdded(item: Item, matsideNav: MatSidenav) {
        this.snackbarSubscription = this.matsnackbar.open(`${item.name} added to your cart.`, matsideNav.opened ? '' : 'Open Cart').onAction().subscribe(() => {
            matsideNav.open();
        });
    }

    ngOnDestroy(): void {
        this.snackbarSubscription?.unsubscribe();
    }

}
