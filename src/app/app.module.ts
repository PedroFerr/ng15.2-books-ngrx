import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { environment } from 'src/environments/environment';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { shellReducer } from './shell/core.reducer';


import { AppMaterialModule } from './app-material.module';
import { CartModule } from './features-modules/shopping-cart/cart.module';
import { CatalogModule } from './features-modules/catalog/catalog.module';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppMaterialModule,
        StoreModule.forRoot(shellReducer),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production, // Restrict extension to log-only mode
            autoPause: true, // Pauses recording actions and state changes when the extension window is not open
        }),
        CatalogModule,
        CartModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
