import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from "@ngrx/store";
import { EffectsModule } from '@ngrx/effects';

import {CartComponent} from './cart/cart.component';
import {CartItemComponent} from './cart/cart-item/cart-item.component';
import {cartReducer} from './store/cart.reducer';
import {NgPipesModule} from "ngx-pipes";
import {CartEffect} from './store/cart.effect';
import { AppMaterialModule } from 'src/app/app-material.module';

@NgModule({
  declarations: [
    CartComponent,
    CartItemComponent
  ],
  exports: [
    CartComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    EffectsModule.forFeature([CartEffect]),
    StoreModule.forFeature('cartFeature', cartReducer),
    NgPipesModule
  ]
})
export class CartModule {
}
