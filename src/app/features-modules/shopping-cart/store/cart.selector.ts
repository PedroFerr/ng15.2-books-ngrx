import { createSelector } from '@ngrx/store';
import { CartItem } from 'src/app/app-interfaces';

export interface CartFeatureState {
    cartItems: CartItem[],
    numberOfItems: number
}

export const selectCartState = (state: CartFeatureState) => state;

export const selectCartItems = createSelector(
    selectCartState,
    (state: any | undefined) => {
        return state?.cartFeature?.cartItems;
    }
);

export const selectCartTotalPrice = createSelector(
    selectCartState,
    (state: any | undefined) => {
        return state.cartFeature?.cartItems.reduce((accumulator: number, cartItem: CartItem) => {
            console.info(accumulator, cartItem.qty)
            return accumulator + (cartItem.qty * cartItem.item.price);
        }, 0);
    }
);
