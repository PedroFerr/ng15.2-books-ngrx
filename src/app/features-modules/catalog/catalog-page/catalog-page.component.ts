import { Component } from '@angular/core';
import { ItemsFeatureState, selectItems } from '../store/catalog.selector';
import { Store } from '@ngrx/store';
import { CatalogPageActions } from '../store/catalog-page.actions';
import { Item } from 'src/app/app-interfaces';

@Component({
    selector: 'app-catalog-page',
    templateUrl: './catalog-page.component.html',
    styleUrls: ['./catalog-page.component.scss']
})
export class CatalogPageComponent {

    items$ = this.store.select(selectItems);

    constructor(private readonly store: Store<ItemsFeatureState>) {
        this.store.dispatch(CatalogPageActions.getItems());
    }

    addItemToCart(item: Item) {
        this.store.dispatch(CatalogPageActions.addItemToCart({ item }));
    }

}
