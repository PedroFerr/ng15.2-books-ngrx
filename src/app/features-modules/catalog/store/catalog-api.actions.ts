import {createAction, props} from '@ngrx/store';
import { Item } from 'src/app/app-interfaces';

export const itemsLoadedSuccess = createAction(
  '[Items API] Items loaded',
  props<{ data: Item[] }>()
);
