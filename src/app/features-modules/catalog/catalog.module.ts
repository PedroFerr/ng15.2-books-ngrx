import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EffectsModule} from "@ngrx/effects";
import {CatalogEffect} from "./store/catalog.effect";
import {StoreModule} from "@ngrx/store";
import {catalogReducer} from "./store/catalog.reducer";
import {HttpClientModule} from "@angular/common/http";

import { AppMaterialModule } from 'src/app/app-material.module';

import { MatCardModule } from '@angular/material/card';
import { CatalogPageComponent } from './catalog-page/catalog-page.component';


@NgModule({
  declarations: [
    CatalogPageComponent,
  ],
  exports: [
    CatalogPageComponent
  ],
    imports: [
        CommonModule,
        HttpClientModule,
        AppMaterialModule,
        StoreModule.forFeature('itemsFeature', catalogReducer),
        EffectsModule.forFeature([CatalogEffect])
    ]
})
export class CatalogModule { }
